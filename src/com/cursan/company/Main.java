package com.cursan.company;

import org.jpl7.Query;
import org.jpl7.*;

import java.io.IOException;
import java.lang.Integer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.lang.String;

import static java.nio.file.StandardOpenOption.APPEND;

public class Main {

     static Path ctPath1 = Paths.get("Symptomes1.csv");
     static Path ctPath2 = Paths.get("Symptomes2.csv");
     static Path ctPathProl = Paths.get("Base de connaissance.pl");


    /**
     * Fonction qui répertorie toutes les questions dans le cas où l'ordinateur ne s'allume pas
     * @return questions, un tableau de questions
     */
    public static ArrayList<Questions> initialize_question1(){
        List<String> lines = null;
        ArrayList<Questions> q = new ArrayList<>();
        try{
            lines = Files.readAllLines(ctPath1);
        }
        catch (IOException e){
            System.out.println("impossible de lire le fichier");
        }

        for (int i=1; i<lines.size();i++){
            String[] split = lines.get(i).split(",");
            Questions r = new Questions(split[0], split[1]);
            q.add(r);

        }
        return q;
    }

    /**
     * Fonction qui répertorie toutes les questions dans le cas où l'ordinateur s'allume
     * @return questions, un tableau de questions
     */
    public static ArrayList<Questions> initialize_question2(){
        List<String> lines = null;
        ArrayList<Questions> q = new ArrayList<>();
        try{
            lines = Files.readAllLines(ctPath2);
        }
        catch (IOException e){
            System.out.println("impossible de lire le fichier");
        }

        for (int i=1; i<lines.size();i++){
            String[] split = lines.get(i).split(",");
            Questions r = new Questions(split[0], split[1]);
            q.add(r);
        }
        return q;

    }

    public static void main(String[] args) {

        /*
        Premiere chose a faire avant tout autre chose dans le projet, lire le fichier prolog
         */

        Query n = new Query("consult('Base de connaissance.pl')");
        n.hasSolution();

        // write your code here

        String rep;
        String answer;
        int ans, answ;
        ArrayList<Questions> q = new ArrayList<>();


        System.out.println("*****Bienvenue sur Visual Troubleshooter***** \n \n Ce programme vous permet de détecter quelques pannes de votre ordinateur \n ");
        System.out.println("Voulez-vous effectuer un diagnostic ou ajouter une panne?  Appuyez sur 1 pour faire un diagnostic et 2 pour ajouter une panne");
        Scanner src = new Scanner(System.in);
        ans = src.nextInt();

        if (ans ==1){ //si on fait le diagnostic
            System.out.println("Votre ordinateur reste-t-il éteint lorsque vous pressez le bouton d'allumage?");
            Scanner sp = new Scanner(System.in);
            answer = sp.next();

            if( answer.equals("oui")){
                q = initialize_question1();
            }
            else {
                q = initialize_question2();
            }

            boucle:  for(Questions i: q){
                System.out.println(i.getQuestion());
                Scanner sc = new Scanner(System.in);
                rep = sc.next();

                if (rep.equals("oui")){

                    Query Q = new Query("assert("+i.getCode()+")");
                    Q.hasSolution();

                    Query q1 = new Query("sol(X)");

                    Map<String, Term>[] solutions = q1.allSolutions();
                    if(q1.hasSolution()){
                        for(int j=0; j<solutions.length;j++){
                            System.out.println("Solution ==> "+solutions[j].get("X").toString());
                            break boucle;
                        }
                    }
                }

            }
        }
        else // si on veut plutôt ajouter une panne
            {
                System.out.println("Est-ce en rapport avec le fait que l'ordinateur ne parvienne pas à s'allumer? Ecrivez '1' pour 'oui' et '2' pour 'non' ");
                Scanner t = new Scanner(System.in);
                answ = t.nextInt();
                Path csvPath = null;
                if(answ == 1) // l'ordinateur ne parvient pas à s'allumer
                    csvPath = ctPath1;
                else
                    csvPath = ctPath2;

                String reponse; // récupère l'énoncé de la solution
                System.out.println("Entrez l'intitulé de la solution");
                Scanner sol = new Scanner(System.in);
                reponse = sol.nextLine();
                String a,b; // récupère les différentes questions à poser
                int compteur = initialize_question1().size() + initialize_question2().size();
                ArrayList<String> symptome = new ArrayList<>();

                do {

                    System.out.println("Veuillez saisir une question qui permettrait d'aboutir à la solution. Appuyez sur '0' pour boucler la liste de symptômes");
                    Scanner y = new Scanner(System.in);
                    a = y.nextLine();
                    if (a.equals("0")){
                        break;
                    }

                    compteur++;

                    try{
                        Files.write(csvPath, String.format("\ns"+compteur+","+a).getBytes(), APPEND); //écriture dans le fichier Sysmptomes1.csv
                        symptome.add("s"+compteur);
                    }
                    catch (IOException e){
                        System.out.println("une erreur est survenue. Yako!");
                        return;
                    }
                } while (a.equals("0") == false);

                int num = ((int)(Math.random()*1000))%437;
                try {
                    Files.write(ctPathProl, String.format("sol(\""+reponse+"\") :- p"+num+".\n").getBytes(), APPEND); //écriture de la solution dans le fichier prolog

                    for(String s: symptome){
                        Files.write(ctPathProl, String.format(s+" :- fail.\n").getBytes(), APPEND); // écriture des symptomes dans le fichier
                    }

                    String listeSymptome = String.join(",", symptome);
                    Files.write(ctPathProl, String.format("p"+num+" :- "+listeSymptome+".\n").getBytes(), APPEND);
                }
                catch (IOException e){
                    System.out.println("une erreur est survenue. Yako!");
                    return;
                }
                System.out.println("Enregistrement effectué avec succès!");

        }
    }
}
