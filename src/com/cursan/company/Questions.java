package com.cursan.company;

import java.util.ArrayList;

public class Questions {
    String code;
    String question;


    public Questions(String c, String q){
        code = c;
        question = q;
    }

    String getCode(){
        return code;
    }

    String getQuestion(){
        return question;
    }


}
